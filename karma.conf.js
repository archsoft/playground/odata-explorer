module.exports = function(config) {
    config.set({
      files: [
        'webapp/test/integration/BusyJourney.js'
      ],
      browsers: ["ChromeDev"],
      customLaunchers: {
        ChromeDev: {
          base: 'Chrome',
          flags: ['--auto-open-devtools-for-tabs'],
          browserNoActivityTimeout: 999986400,
          clearContext: false,
        }
      },
      proxyValidateSSL: false,
      proxies: {
          '/sap/opu/odata/': 'http://localhost:3000/sap/opu/odata/'
      },
      client: {
        clearContext: false,
        qunit: {
          showUI: true,
          testTimeout: 30000,
          autostart: false
        },
        "ui5-toolkit":{
          mock: config.mock !== 'true'
        },
      },
      logLevel: config.LOG_WARNLOG_WARN,
      browserConsoleLogOptions: { level: 'warn' },
      frameworks: ['qunit','ui5-toolkit', ],

  })
  };
