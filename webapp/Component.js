sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"./model/models",
	"sap/ui/model/json/JSONModel",
	"./controller/util/Service",
], function (UIComponent, Device, models, JSONModel, UtilService) {
	"use strict";

	return UIComponent.extend("de.archsoft.playground.odata.explorer.Component", {

		metadata : {
			manifest : "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * In this method, the device models are set and the router is initialized.
		 * @public
		 * @override
		 */
		init : function () {
			//this.oListSelector = new ListSelector();
			//this._oErrorHandler = new ErrorHandler(this);

			// set the device model
			this.setModel(models.createDeviceModel(), "device");
			 const oModel = new JSONModel({
				_layout : "",
				previousLayout : "",
				currentRoute: "",
				data:{

				}
			})
			this.setModel(oModel, "app");
			this.setModel(oModel, "appRoot");

			// call the base component's init function and create the App view
			UIComponent.prototype.init.apply(this, arguments);


			this.getRouter().attachBeforeRouteMatched((oEvent)=>{
				oModel.setProperty('/currentRoute', oEvent.getParameter('name'))
			})

			// create the views based on the url/hash
			this.getRouter().initialize();

			//UtilService.createInstance(this.getOwnerComponent())


			
		},

		/**
		 * The component is destroyed by UI5 automatically.
		 * In this method, the ListSelector and ErrorHandler are destroyed.
		 * @public
		 * @override
		 */
		destroy : function () {
			
			// call the base component's destroy function
			UIComponent.prototype.destroy.apply(this, arguments);
		},


	});
});
