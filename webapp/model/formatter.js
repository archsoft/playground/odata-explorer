sap.ui.define([], function () {
	"use strict";

	return {
		/**
		 * Rounds the currency value to 2 digits
		 *
		 * @public
		 * @param {string} sValue value to be formatted
		 * @returns {string} formatted currency value with 2 digits
		 */
		currencyValue : function (sValue) {
			if (!sValue) {
				return "";
			}

			return parseFloat(sValue).toFixed(2);
		},

		entityTypeKeyProperties: function(oKey){
			if(oKey){
				return oKey.propertyRef.map((v)=>{
					return v.name;
				}).join(", ")
			}
			return ""
		},
		entityTypePropertyIsKey: function(oRoot, item){
			if(oRoot && item){
				return oRoot.key.propertyRef.findIndex((entry)=>{
					return entry.name === item.name
				}) !== -1
				}
			return false
		}
	};
});