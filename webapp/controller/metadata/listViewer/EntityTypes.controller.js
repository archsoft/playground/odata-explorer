
sap.ui.define([
	"./../../BaseController",
	"sap/ui/model/json/JSONModel",
	"./../../../model/formatter"
], function (BaseController, JSONModel, formatter) {
	"use strict";

	return BaseController.extend("de.archsoft.playground.odata.explorer.controller.metadata.listViewer.EntityTypes", {

		formatter: formatter,

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		onInit : function () {
			// Model used to manipulate control states. The chosen values make sure,
			// detail page is busy indication immediately so there is no break in
			// between the busy indication for loading the view's meta data sapFFCLNavigationButton
			var oViewModel = new JSONModel({
				busy : false,
				delay : 0,
				lineItemListTitle : this.getResourceBundle().getText("detailLineItemTableHeading")
			});
			this.setModel(oViewModel, "detailView");

			
			this.getRouter().getRoute("metadata-listViewer").attachPatternMatched(this._onObjectMatched, this);
			this.getRouter().getRoute("properties").attachPatternMatched(this._onObjectMatched, this);
			//this._oEventBus = sap.ui.getCore().getEventBus();
			
			//this._oEventBus.subscribe("_args", this._bindView, this);			
		},

	
		/**
		 * Binds the view to the object path and expands the aggregated line items.
		 * @function
		 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
		 * @private
		 */
		_onObjectMatched : function (oEvent) {
			const oArguments = oEvent.getParameter("arguments")			
			
			this.getModel().metadataLoaded().then( ()=> {
				//this._oEventBus.publish("_args", oArguments );
				this._bindView(oArguments)
			});
		},

		/**
		 * Binds the view to the object path. Makes sure that detail view displays
		 * a busy indicator while data for the corresponding element binding is loaded.
		 * @function
		 * @param {string} sObjectPath path to the object to be bound to the view.
		 * @private
		 */
		_bindView : function (oData) {
			this._args = oData
		},

	

		
		onItemPPress: function(oEvent){
			const oItem = oEvent.getParameter("listItem") || oEvent.getSource()
			//this.getModel("app").setProperty("/layout", "ThreeColumnsMidExpanded");
			//this.getModel("app").setProperty("/layout", "ThreeColumnsEndExpanded");
			this.getRouter().navTo("properties", {
				ID : this._args.ID,
				Entity: oItem.getBindingContext('app').getPath().split("/").slice(-1).join()
			});
		}
	});

});