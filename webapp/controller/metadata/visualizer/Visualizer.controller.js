


sap.ui.define([
	"./../../BaseController",
	"sap/ui/model/json/JSONModel",
	"./../../../model/formatter",
	"./../../util/Service",
], function (BaseController, JSONModel, formatter, Service) {
	"use strict";

	return BaseController.extend("de.archsoft.playground.odata.explorer.controller.metadata.visualizer.Visualizer", {

		formatter: formatter,



		onInit : function () {
			this._oViewModel = new JSONModel({
				busy: false,
				source: "atomicCircle",
				orientation: "LeftRight",
				arrowPosition: "End",
				arrowOrientation: "ParentOf",
				nodeSpacing: 40,
				nodePlacement: "Simple",
				mergeEdges: false
			});
			this.setModel(this._oViewModel, "ui")

			// graph model must be root!!!
			this._oGraphModel = new JSONModel({
				nodes: [],
				lines: []
			});


			this.getRouter().getRoute("metadata-visualizer").attachPatternMatched(this._onObjectMatched, this);	
			this.getView().setModel(this._oGraphModel);
		},

	
		/**
		 * Binds the view to the object path and expands the aggregated line items.
		 * @function
		 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
		 * @private
		 */
		_onObjectMatched : function (oEvent) {
			Service.getInstance(this.getOwnerComponent()).getSchema().then((schema)=>{
				
				// add some delay flexible column layout is very slow/bad timed
				setTimeout(()=>{
					this._setGraphModel(schema)
				}, 500);
			})

			/*
			const oArguments = oEvent.getParameter("arguments")			
			
			this.getModel().metadataLoaded().then( ()=> {
				//this._oEventBus.publish("_args", oArguments );
				this._bindView(oArguments)
			});
			*/
		},

		/**
		 * Binds the view to the object path. Makes sure that detail view displays
		 * a busy indicator while data for the corresponding element binding is loaded.
		 * @function
		 * @param {string} sObjectPath path to the object to be bound to the view.
		 * @private
		 */
		_bindView : function (oData) {
			this._args = oData;
			
			//const oModel = this.getModel('app');
			
			const path = "/data/schema/entityType/"+oData.Entity+"/";
			this.getView().bindElement({
				path : "app>"+path,	
			});
			this.getView().bindElement({
				path : "appRoot>"+path,	
			});
		},

		exportSvg: function(fnCallback) {
			var domSvg = this.getView().byId("graph").$().find("svg")[0],
				domCopy = domSvg.cloneNode(true);

			// apply inline CSS styles
			this._copyStylesInline(domCopy, domSvg);

			fnCallback(domCopy, domSvg);
		},

		formatIntValue: function(sValue) {
			return parseInt(sValue, 10);
		},

		formatBoolValue: function(sValue) {
			return (sValue === "1");
		},

		formatPropertyName: function(sEntityType, sProperty) {
			return this.getText("@" + sEntityType + ((sProperty) ? sProperty : ""));
		},

		_resolveAliasName: function(sName) {
			return (sName) ? sName.substr(sName.lastIndexOf(".") + 1) : sName;
		},

		_setGraphModel: function(oSchema) {
			var that = this,
				aAssociation = oSchema.association,
				//aKeys, sType,
				aKeys,
				aNodes = [],
				aAttributes = [],
				aLines = [],
				aLineKeys = [];

			if (oSchema.entityType) {
				oSchema.entityType.forEach(function(oEntity) {
					aKeys = [];
					aAttributes = [];

					// keys
					oEntity.key.propertyRef.forEach(function(oKey) {
						aKeys.push(oKey.name);
					});

					// iterate property(s)
					if (oEntity.property) {
						oEntity.property.forEach(function(oProperty) {
							// defaults
							oProperty.label = "-";

							if (oProperty.extensions) {
								oProperty.extensions.forEach(function(oExtension) {
									switch (oExtension.name) {
										case "label":
											oProperty.label = oExtension.value;
											break;
									}
								});
							}

							//sType = (oProperty.type === "Edm.String") ? "" : (" (" + oProperty.type.substr(4) + ")");

							aAttributes.push({
								//"label": that.getText(oProperty.label.substr(7, oProperty.label.length - 1 - 7)),
								//"label": oProperty.name + sType,
								"label": oProperty.name,
								"value": oProperty.type.substr(4)
								//"value": that.getText(oProperty.label.substr(7, oProperty.label.length - 1 - 7))
							});
						});
					}

					aAttributes.push({
						"label": ""
					});

					// iterate navigationProperty(s)
					if (oEntity.navigationProperty && aAssociation && aAssociation.length > 0) {
						oEntity.navigationProperty.forEach(function(oNavigationProperty) {
							var sAssociationName = that._resolveAliasName(oNavigationProperty.relationship);

							// find corresponding item(s)
							var aTarget = jQuery.grep(aAssociation, function(oAssociation) {
								return oAssociation.name === sAssociationName;
							});

							if (aTarget && aTarget.length > 0) {
								var oAssociation = aTarget[0];

								// resolve start type
								var aEndTarget = jQuery.grep(oAssociation.end, function(oEnd) {
									return oEnd.role === oNavigationProperty.fromRole;
								});

								if (aEndTarget && aEndTarget.length > 0) {
									oNavigationProperty.fromType = that._resolveAliasName(aEndTarget[0].type);
								}

								// resolve end type
								aEndTarget = jQuery.grep(oAssociation.end, function(oEnd) {
									return oEnd.role === oNavigationProperty.toRole;
								});

								if (aEndTarget && aEndTarget.length > 0) {
									oNavigationProperty.toType = that._resolveAliasName(aEndTarget[0].type);
								}

								if (aLineKeys.indexOf(oNavigationProperty.fromRole + "_" + oNavigationProperty.toRole) === -1) {
									aLines.push({
										from: oNavigationProperty.fromType,
										to: oNavigationProperty.toType
									});
									aLineKeys.push(oNavigationProperty.fromRole + "_" + oNavigationProperty.toRole);
									aAttributes.push({
										label: oNavigationProperty.name,
										value: oNavigationProperty.toType
									});
								}
							}
						});
					}

					aNodes.push({
						"key": oEntity.name,
						//"title": oEntity.name + " (" + aKeys.join(",") + ")",
						"title": oEntity.name,
						//"description": that.getText("@" + oEntity.name.toString() + "TypeDescription"),
						"description": aKeys.join(","),
						"icon": "sap-icon://form",
						"shape": "Box",
						"status": "Success",
						"attributes": aAttributes
					});
				});
			}

			this._oGraphModel.setProperty("/nodes", aNodes);
			this._oGraphModel.setProperty("/lines", aLines);
		},
	

		
		
	});

});