sap.ui.define([
	"./BaseController"
], function (BaseController) {
	"use strict";

	return BaseController.extend("de.archsoft.playground.odata.explorer.controller.DetailObjectNotFound", {});
});