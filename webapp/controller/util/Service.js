sap.ui.define([
    "sap/ui/base/Object",
	"sap/ui/model/odata/v2/ODataModel",
	"sap/base/Log"
], function (bObject, ODataModelV2, Log) {
	"use strict";
	
	let instance = null
    let Service =  bObject.extend("de.archsoft.playground.odata.explorer.controller.util.Service", {

        constructor: function(oC){
			this._component = oC
			this._lazy = new Promise((resolve, reject)=>{
				this._lazyResolve = resolve
				this._lazyReject = reject
				
			})
		},
		
        loadService: function ( sServiceUrl) {
			this.myPromise =  new Promise((resolve, reject) => {
				
				// /sap/opu/odata/
				// should stay compatible with a traditional gateway system
				//"https://gwaas-p1349906996trial.hanatrial.ondemand.com/odata/IWBEP/GWSAMPLE_BASIC;v=1"

				let sServicePath = sServiceUrl.substr(sServiceUrl.indexOf('.com/odata/')+10)
				 sServiceUrl = "/sap/opu/odata" +sServicePath

				this._oODataModel = new ODataModelV2(sServiceUrl, { });

				// handle metadata load
				this._oODataModel.metadataLoaded().then( (oEvent)=> {
					const oServiceMetadata = this._oODataModel.getServiceMetadata()
					this._component.getModel('app').setProperty("/data/schema", oServiceMetadata.dataServices.schema[0])

					resolve(oServiceMetadata.dataServices.schema[0])
					this._lazyResolve(oServiceMetadata.dataServices.schema[0])
				});
				// handle metadata error
				this._oODataModel.attachMetadataFailed( (oEvent)=> {
					reject()
					this._lazyReject()
				});
			});
			return this.myPromise
		},
		
		getSchema: function(){
			return this._lazy
		}

	});
	
	
	return {
		createNewInstance: function(oComponent){
			instance = new Service(oComponent)
		},
		getInstance: function(oComponent){
			if ( !instance && !oComponent ){
				Log.fatal("supply a component")
			}else if (!instance ){
				instance = new Service(oComponent)
			}
			return instance
		}
	}

});

