
sap.ui.define([
	"./../BaseController",
	"sap/ui/model/json/JSONModel",
	"./../../model/formatter",
	"./../util/Service",
	'sap/m/MessageBox'
], function (BaseController, JSONModel, formatter, UtilService,MessageBox) {
	"use strict";

	return BaseController.extend("de.archsoft.playground.odata.explorer.controller.aspect.Aspect", {

		formatter: formatter,

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		onInit : function () {
			var oViewModel = new JSONModel({
				busy : false,
				delay : 0,
				lineItemListTitle : this.getResourceBundle().getText("detailLineItemTableHeading")
			});
			this.setModel(oViewModel, "detailView");

			const oRouter = this.getRouter();
			oRouter.getRoute("aspect").attachPatternMatched(this._onObjectMatched, this);
			oRouter.getRoute("metadata-listViewer").attachPatternMatched(this._onObjectMatched, this);
			oRouter.getRoute("metadata-visualizer").attachPatternMatched(this._onObjectMatched, this);
			oRouter.getRoute("properties").attachPatternMatched(this._onObjectMatched, this);				
		},

	
		/**
		 * Binds the view to the object path and expands the aggregated line items.
		 * @function
		 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
		 * @private
		 */
		_onObjectMatched : function (oEvent) {
			const oArguments = oEvent.getParameter("arguments")			
			//this.getModel("app").setProperty("/layout", "TwoColumnsMidExpanded");
			this.getModel().metadataLoaded().then( ()=> {
				//this._oEventBus.publish("_args", oArguments );
				this._bindView(oArguments)
				
			});
		},



		/**
		 * Binds the view to the object path. Makes sure that detail view displays
		 * a busy indicator while data for the corresponding element binding is loaded.
		 * @function
		 * @param {string} sObjectPath path to the object to be bound to the view.
		 * @private
		 */
		_bindView : function (oData) {
			this._args = oData
			const oModel = this.getModel()
			const sObjectPath = '/'+ oModel.createKey("ServiceCollection", {
				ID :  oData.ID
			});
			this.getView().setBusy(true)
			this.getView().setBlocked(false)
			this.getView().bindElement({
				path : sObjectPath,
				events: {
					change: (oEvent)=>{
						const sServiceUrl = this.getView().getBindingContext().getObject().ServiceUrl
						const serviceUtil = UtilService.getInstance(this.getOwnerComponent())
						serviceUtil.loadService(sServiceUrl)

						//this._utilService =  UtilService.createInstance(this.getOwnerComponent(), sServiceUrl)
						.then(()=>{
							this.getView().setBusy(false)
						}).catch(()=>{
							this.getView().setBusy(false)
							this.getView().setBlocked(true)
							MessageBox.error(
								"Could not load metadata. Check the service: "+ sServiceUrl
							);
						})
					},
				}
			});


		},

		onItemPress: function(oEvent){
			//const oItem = oEvent.getParameter("listItem") || oEvent.getSource()
			//this.getModel("app").setProperty("/layout", "ThreeColumnsMidExpanded");
			//this.getModel("app").getProperty("/currentLayout", "ThreeColumnsEndExpanded");

			var oRouter = this.getRouter();
			var sLocalID = oEvent.getParameter('listItem').getId().split('--').slice(-1)[0]


			if (sLocalID === "metVis"){
				oRouter.navTo("metadata-visualizer", this._args, false);
			}else if (sLocalID === "metList"){
				if(this.getModel("app").getProperty("/currentRoute")==="properties"){
					oRouter.navTo("properties", this._args), false;
				}else{
					oRouter.navTo("metadata-listViewer", {
						ID : this._args.ID
					}, false);
				}
			}

			
			
		}

	

	
	});

});