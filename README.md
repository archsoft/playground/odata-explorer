<!-- TOC -->

- [1. OData Explorer](#1-odata-explorer)
- [2. Destination Mapping](#2-destination-mapping)
    - [2.1. GWAAS (Default)](#21-gwaas-default)
    - [2.2. OnPremise System](#22-onpremise-system)
- [3. Demo](#3-demo)
- [4. Side-note](#4-side-note)

<!-- /TOC -->

# 1. OData Explorer
  - Explore your oData service with this app. 
  - This app serves also as demo for the ui5-toolkit v2.

# 2. Destination Mapping

## 2.1. GWAAS (Default)
```json

    {
      "path": "/sap/opu/odata/iwfnd/CATALOGSERVICE/",
      "target": {
        "type": "destination",
        "name": "ES5_GWAAS",
        "entryPath": "/CATALOGSERVICE/"
      },
      "description": "SAP Gateway Demo System - GWAAS"
    },
    {
      "path": "/sap/opu/odata/",
      "target": {
        "type": "destination",
        "name": "ES5_GWAAS",
        "entryPath": "/odata/"
      },
      "description": "SAP Gateway Demo System - GWAAS"
    },"description": "SAP Gateway Demo System"
    }
``` 


## 2.2. OnPremise System
Adjust neo-app.json target for Gateway Systems
```json
{
    "path": "/sap/opu/",
    "target": {
    "type": "destination",
    "name": "ES5_GWAAS",
    "entryPath": "/sap/opu/"
    },
    "description": "SAP Gateway Demo System"
}
```
   

# 3. Demo
At the time of writing, there is no way to make GWAAS services public.

Therefore, you must deploy it to your own infrastructure. Not working^^: [demo](https://dearchsoftplaygroundodataexplo-p1349906996trial.dispatcher.hanatrial.ondemand.com/webapp/index.html?).

![Sample Viode](https://gitlab.com/archsoft/playground/odata-explorer/raw/master/doc/demo.webm)


# 4. Side-note
This is a port from: https://github.com/hschaefer123/odataexplorer
